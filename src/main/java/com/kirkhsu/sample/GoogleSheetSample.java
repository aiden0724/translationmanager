package com.kirkhsu.sample;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GoogleSheetSample {
	private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
    private static final String CREDENTIALS_FILE_PATH = "/client_secret_447195449395-dmglejb2kllon48e90hsfpjn1aj36gqi.apps.googleusercontent.com.json";
	
    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = GoogleSheetSample.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
    
    private static void whenWriteStringUsingBufferedWritter_thenCorrect(String str, String fileName) throws IOException {
    	String directoryName = fileName.substring(0, fileName.lastIndexOf("/"));
    	Path path = Paths.get(directoryName);
    	Files.createDirectories(path);

	    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, StandardCharsets.UTF_8));
	    writer.write(str);
	    writer.close();
	}
    
    private static void writeXml(Element e, String fileName) throws Exception{
    	String directoryName = fileName.substring(0, fileName.lastIndexOf("/"));
    	Path path = Paths.get(directoryName);
    	Files.createDirectories(path);
    	
		XMLOutputter outter = new XMLOutputter();
		outter.setFormat(Format.getPrettyFormat());
		outter.output(new Document(e), new FileWriter(new File(fileName), StandardCharsets.UTF_8));
    }
    
    private static String trimValueStringForIOS(String ss) {
    	String[] values = ss.split("\n");
    	if(values.length>1) {
    		StringBuffer sb = new StringBuffer();
    		for(int i=0; i<values.length; i++) {
    			if(i>0) {
    				sb.append("\\n");
    			}
    			sb.append(values[i]);
    		}
    		return sb.toString().replaceAll("\"", "\\\\\"").trim();
    	}else {
    		return ss.replaceAll("\"", "\\\\\"").trim();
    	}
    }
    
    private static String trimValueStringForAndroid(String ss) { 
    	String[] values = ss.split("\n");
    	if(values.length>1) {
    		
    		StringBuffer sb = new StringBuffer();
    		for(int i=0; i<values.length; i++) {
    			if(i>0) {
    				sb.append("\\n");
    			}
    			sb.append(values[i]);
    		}
    		return sb.toString().replaceAll("XXXXX", "\\%1\\$s").trim();
    	}else {
    		return ss.replaceAll("XXXXX", "\\%1\\$s").trim();
    	}
    }
    
	private static void copyFileUsingFileStreams(File source, File dest) throws IOException {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} finally {
			input.close();
			output.close();
		}
	}
    
    // https://docs.google.com/spreadsheets/d/1KEpsHaf3QE7tAwNHYeu4tRvEbH0C9axcVTRfff32G6g/edit?usp=sharing
	static public void main(String[] args) throws Exception {
		
		final java.util.logging.Logger buggyLogger = java.util.logging.Logger.getLogger(FileDataStoreFactory.class.getName());
        buggyLogger.setLevel(java.util.logging.Level.SEVERE);
		
		String sheetId = "1KEpsHaf3QE7tAwNHYeu4tRvEbH0C9axcVTRfff32G6g";
        String sheetName = "APP text_new_V2.1";
        
        String iosPath = null;
        String androidPath = null;
        
        if(args.length > 0) {
        	for(String arg : args) {
        		String[] argValues = arg.split("=");
        		if(argValues.length != 2) continue;
        		if("sheetId".equals(argValues[0])) {
        			sheetId = argValues[1];
        			System.out.println("sheetId:" + sheetId);
        		}else if("iosPath".equals(argValues[0])) {
        			iosPath = argValues[1];
        			System.out.println("iosPath:" + iosPath);
        		}else if("androidPath".equals(argValues[0])) {
        			androidPath = argValues[1];
        			System.out.println("androidPath:" + androidPath);
        		}
        	}
        }
		
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        String range = sheetName + "!A1:S";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
        ValueRange response = service.spreadsheets().values().get(sheetId, range).execute();
        List<List<Object>> values = response.getValues();
        
        String commonRange = "APP text_common!A1:S";
        ValueRange commonResponse = service.spreadsheets().values().get(sheetId, commonRange).execute();
        List<List<Object>> commonValues = commonResponse.getValues();
        
        String iOSonlyRange = "iOS_only_text!A2:K";
        ValueRange iOSonlyResponse = service.spreadsheets().values().get(sheetId, iOSonlyRange).execute();
        List<List<Object>> iOSonlyValues = iOSonlyResponse.getValues();
        
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            //System.out.println("Name, Major");
        	
        	//Root Element
        	Element frRootElement = new Element("resources");
        	Element enRootElement = new Element("resources");
        	Element gerRootElement = new Element("resources");
        	Element nlRootElement = new Element("resources");
        	Element dkRootElement = new Element("resources");
        	Element itRootElement = new Element("resources");
        	Element esRootElement = new Element("resources");
        	Element ptRootElement = new Element("resources");
        	Element cnsRootElement = new Element("resources");
        	Element cntRootElement = new Element("resources");
        	
        	String ourputHeader = "/*\n"+ " Localizable.strings\n probe\n Created by PCP Probe translation generator on " + sdFormat.format(new Date()) + ".\n Copyright 2020 PCPartner. All rights reserved." + "\n */\n\n";
        	ourputHeader = new String(ourputHeader.getBytes(), StandardCharsets.UTF_8);
        	StringBuffer frBuffer = new StringBuffer(ourputHeader);
        	StringBuffer enBuffer = new StringBuffer(ourputHeader);
        	StringBuffer gerBuffer = new StringBuffer(ourputHeader);
        	StringBuffer nlBuffer = new StringBuffer(ourputHeader);
        	StringBuffer dkBuffer = new StringBuffer(ourputHeader);
        	StringBuffer itBuffer = new StringBuffer(ourputHeader);
        	StringBuffer esBuffer = new StringBuffer(ourputHeader);
        	StringBuffer ptBuffer = new StringBuffer(ourputHeader);
        	StringBuffer cnsBuffer = new StringBuffer(ourputHeader);
        	StringBuffer cntBuffer = new StringBuffer(ourputHeader);
        	
            for (List<Object> row : values) {
            	String androidKey = row.get(3).toString();
            	String iosKey = row.get(4).toString();
            	
            	if("iOS key name".equals(iosKey)) {
            		continue;
            	}

            	String frValue = row.get(5).toString();
            	String enValue = row.get(6).toString();
            	String gerValue = row.get(7).toString();
            	String nlValue = row.get(8).toString();
            	String dkValue = row.get(9).toString();
            	String itValue = row.get(10).toString();
            	String esValue = row.get(11).toString();
            	String ptValue = row.get(12).toString();
            	String cnsValue = row.get(13).toString();
            	String cntValue = row.get(14).toString();
            	
            	// iOS
            	if(!"".equals(iosKey)) {
            		for(String key : iosKey.split("\n")) {
            			
                    	if(key.contains("(x)")) {
                    		continue;
                    	}
            			
            			frBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(frValue)));
            			enBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(enValue)));
            			gerBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(gerValue)));
            			nlBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(nlValue)));
            			dkBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(dkValue)));
            			itBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(itValue)));
            			esBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(esValue)));
            			ptBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(ptValue)));
            			cnsBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cnsValue)));
            			cntBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cntValue)));
            		}
            	}
            	
            	// android
            	if(!"".equals(androidKey)) {
            		for(String key : androidKey.split("\n")) {
            			
            			if(key.contains("(") || key.contains("<") || key.contains(" ")) {
                    		continue;
                    	}
            			
            			frRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(frValue)));
            			enRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(enValue)));
            			gerRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(gerValue)));
            			nlRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(nlValue)));
            			dkRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(dkValue)));
            			itRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(itValue)));
            			esRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(esValue)));
            			ptRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(ptValue)));
            			cnsRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(cnsValue)));
            			cntRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(cntValue)));
            		}
            	}
            }

            for (List<Object> row : commonValues) {
            	String androidKey = row.get(3).toString();
            	String iosKey = row.get(4).toString();
            	
            	if("iOS key name".equals(iosKey)) {
            		continue;
            	}

            	String frValue = row.get(5).toString();
            	String enValue = row.get(6).toString();
            	String gerValue = row.get(7).toString();
            	String nlValue = row.get(8).toString();
            	String dkValue = row.get(9).toString();
            	String itValue = row.get(10).toString();
            	String esValue = row.get(11).toString();
            	String ptValue = row.get(12).toString();
            	String cnsValue = row.get(13).toString();
            	String cntValue = row.get(14).toString();
            	
            	// iOS
            	if(!"".equals(iosKey)) {
            		for(String key : iosKey.split("\n")) {
            			
                    	if(key.contains("(x)")) {
                    		continue;
                    	}
            			
            			frBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(frValue)));
            			enBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(enValue)));
            			gerBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(gerValue)));
            			nlBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(nlValue)));
            			dkBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(dkValue)));
            			itBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(itValue)));
            			esBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(esValue)));
            			ptBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(ptValue)));
            			cnsBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cnsValue)));
            			cntBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cntValue)));
            		}
            	}
            	
            	// android
            	if(!"".equals(androidKey)) {
            		for(String key : androidKey.split("\n")) {
            			
            			if(key.contains("(") || key.contains("<") || key.contains(" ")) {
                    		continue;
                    	}
            			
            			frRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(frValue)));
            			enRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(enValue)));
            			gerRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(gerValue)));
            			nlRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(nlValue)));
            			dkRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(dkValue)));
            			itRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(itValue)));
            			esRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(esValue)));
            			ptRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(ptValue)));
            			cnsRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(cnsValue)));
            			cntRootElement.addContent(new Element("string").setAttribute("name", key).addContent(trimValueStringForAndroid(cntValue)));
            		}
            	}
            }

            if (iOSonlyValues != null && !iOSonlyValues.isEmpty()) {
            	for (List<Object> row : iOSonlyValues) {
                	String iosKey = row.get(0).toString();
                	
                	String frValue = row.get(1).toString();
                	String enValue = row.get(2).toString();
                	String gerValue = row.get(3).toString();
                	String nlValue = row.get(4).toString();
                	String dkValue = row.get(5).toString();
                	String itValue = row.get(6).toString();
                	String esValue = row.get(7).toString();
                	String ptValue = row.get(8).toString();
                	String cnsValue = row.get(9).toString();
                	String cntValue = row.get(10).toString();
                	
                	if(!"".equals(iosKey)) {
                		for(String key : iosKey.split("\n")) {
                			
                        	if(key.contains("(x)")) {
                        		continue;
                        	}
                			
                			frBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(frValue)));
                			enBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(enValue)));
                			gerBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(gerValue)));
                			nlBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(nlValue)));
                			dkBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(dkValue)));
                			itBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(itValue)));
                			esBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(esValue)));
                			ptBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(ptValue)));
                			cnsBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cnsValue)));
                			cntBuffer.append(String.format("\"%s\"=\"%s\";\n", key.trim(), trimValueStringForIOS(cntValue)));
                		}
                	}
            	}
            }
            
            // output to file
            whenWriteStringUsingBufferedWritter_thenCorrect(frBuffer.toString(), "output/iOS/fr.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(enBuffer.toString(), "output/iOS/en.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(enBuffer.toString(), "output/iOS/Base.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(gerBuffer.toString(), "output/iOS/de.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(nlBuffer.toString(), "output/iOS/nl.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(dkBuffer.toString(), "output/iOS/da.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(itBuffer.toString(), "output/iOS/it.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(esBuffer.toString(), "output/iOS/es.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(ptBuffer.toString(), "output/iOS/pt.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(cnsBuffer.toString(), "output/iOS/zh-Hans.lproj/Localizable.strings");
            whenWriteStringUsingBufferedWritter_thenCorrect(cntBuffer.toString(), "output/iOS/zh-Hant.lproj/Localizable.strings");
            
            // copy file to iOS project folder
            // Note: The output folder must exist.
            if(iosPath != null) {
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/fr.lproj/Localizable.strings"), new File(iosPath+"/fr.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/en.lproj/Localizable.strings"), new File(iosPath+"/en.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/Base.lproj/Localizable.strings"), new File(iosPath+"/Base.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/de.lproj/Localizable.strings"), new File(iosPath+"/de.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/nl.lproj/Localizable.strings"), new File(iosPath+"/nl.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/da.lproj/Localizable.strings"), new File(iosPath+"/da.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/it.lproj/Localizable.strings"), new File(iosPath+"/it.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/es.lproj/Localizable.strings"), new File(iosPath+"/es.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/pt.lproj/Localizable.strings"), new File(iosPath+"/pt.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/zh-Hans.lproj/Localizable.strings"), new File(iosPath+"/zh-Hans.lproj/Localizable.strings"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/iOS/zh-Hant.lproj/Localizable.strings"), new File(iosPath+"/zh-Hant.lproj/Localizable.strings"));
            }

            // output android XML
            writeXml(frRootElement, "output/Android/values-fr-rFR/strings.xml");
            writeXml(enRootElement, "output/Android/values-en-rUS/strings.xml");
            writeXml(gerRootElement, "output/Android/values-de-rDE/strings.xml");
            writeXml(nlRootElement, "output/Android/values-nl-rNL/strings.xml");
            writeXml(dkRootElement, "output/Android/values-da-rDK/strings.xml");
            writeXml(itRootElement, "output/Android/values-it-rIT/strings.xml");
            writeXml(esRootElement, "output/Android/values-es-rES/strings.xml");
            writeXml(ptRootElement, "output/Android/values-pt-rPT/strings.xml");
            writeXml(cnsRootElement, "output/Android/values-zh-rCN/strings.xml");
            writeXml(cntRootElement, "output/Android/values-zh-rTW/strings.xml");
            
            // copy file to Android project folder
            // Note: The output folder must exist.
            if(androidPath != null) {
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-fr-rFR/strings.xml"), new File(androidPath+"/values-fr-rFR/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-en-rUS/strings.xml"), new File(androidPath+"/values-en-rUS/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-de-rDE/strings.xml"), new File(androidPath+"/values-de-rDE/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-nl-rNL/strings.xml"), new File(androidPath+"/values-nl-rNL/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-da-rDK/strings.xml"), new File(androidPath+"/values-da-rDK/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-it-rIT/strings.xml"), new File(androidPath+"/values-it-rIT/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-es-rES/strings.xml"), new File(androidPath+"/values-es-rES/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-pt-rPT/strings.xml"), new File(androidPath+"/values-pt-rPT/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-zh-rCN/strings.xml"), new File(androidPath+"/values-zh-rCN/strings.xml"));
            	copyFileUsingFileStreams(new File(Paths.get("").toAbsolutePath() + "/output/Android/values-zh-rTW/strings.xml"), new File(androidPath+"/values-zh-rTW/strings.xml"));
            }
            
            // complete times
            SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
            Date current = new Date();
            System.out.print("The job is completed. " + sdFormat.format(current));
        }
	}
}
